//  Created by Александр Холодов on 9/24/16.
//  Copyright © 2016 Александр Холодов. All rights reserved.

// import Cocoa

// Home work 15.10.16

// checkPrime
func checkPrime (number: Int) -> Bool {
    var div = number - 1
    while div > 1 {
        if number % div == 0 {
            return false
        }
        div -= 1
    }
    return true
}

// checkPrime(number: 88881)

// create arr Prime
func getArrPrime(count: Int) -> [Int] {
    
    var number = 1
    var arrPrime = [Int]()
    
    while arrPrime.count < count {
        if checkPrime(number: number) {
            arrPrime.append(number)
        }
        number += 1
    }
    
    return arrPrime
}

// print(getArrPrime(count: 10))

// print(Int.max)

// create array Fibonacci
func getArrFib (count: Int = 10) -> [Int] {
    
    switch count {
        
    case 1:
        return [1]
        
    case 2..<Int.max:
        var arr = [1,1]
        while (arr.count < count) && ((Int.max - arr[arr.count - 2]) > arr.last!) {
            arr.append(arr[arr.count - 1] + arr[arr.count - 2])
        }
        return arr
        
    default:
        return [0]
        
    }
}

// create array Fibonacci with parameters on default
func getArrFibD (count: Int = 10) -> [Int] {
    
    switch count {
        
    case 1:
        return [1]
        
    case 2..<Int.max:
        var arr = [1,1]
        while (arr.count < count) && ((Int.max - arr[arr.count - 2]) > arr.last!) {
            arr.append(arr[arr.count - 1] + arr[arr.count - 2])
        }
        return arr
        
    default:
        return [0]
        
    }
}

// create array Fibonacci without parameters
func getArrFib () -> [Int] {
    var arr = [1,1]
    while ((Int.max - arr[arr.count - 2]) > arr.last!) {
        arr.append(arr[arr.count - 1] + arr[arr.count - 2])
    }
    return arr
}


// print(getArrFibD())
// print(getArrFib())

// корень квадратный - sqrt
func sqrtInt (number:Int = Int(Int32.max)) -> Int {
    
    var sqrt = Int(number / 2)
    
    while sqrt * sqrt > number {
        sqrt /= 2
    }
    
    while (sqrt + 1) * (sqrt + 1) <= number {
        sqrt += 1
    }
    
    return sqrt
}

print(sqrtInt())

// print(sqrtInt())

// create array Square
func getArrSq (count: Int = 100) -> [Int] {
    
    var arr = [Int]()
    var i = 1
    while (arr.count < count)
        //     && (sqrtInt(number: Int(Int32.max - 1))  > arr.last!)
    {
        arr.append(i * i)
        i += 1
    }
    return arr
}

var count =  20

// print(getArrSq())
let interesingNumbers = [
    "Prime": getArrPrime(count: count),
    "Fibonacci": getArrFib(count: count),
    "Square": getArrSq(count: count),
]

// print(interesingNumbers)

var largest = 0
var kindLargest:String?
for (kind, numbers) in interesingNumbers {
    for number in numbers {
        if number > largest {
            largest = number
            kindLargest = kind
        }
    }
}
//print(largest)
//print(kindLargest)
//print(kindLargest ?? "0")
print("Prime:     " + String(describing: interesingNumbers["Prime"]!))
print("Fibonacci: " + String(describing: interesingNumbers["Fibonacci"]!))
print("Square:    " + String(describing: interesingNumbers["Square"]!))


print("count = \(count), type = " + kindLargest! + ", max = " + String(largest))